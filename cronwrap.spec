%define name %(echo $(${CI_PROJECT_DIR}/package.sh name))
%define release %(echo $(${CI_PROJECT_DIR}/package.sh release))
%define version %(echo $(${CI_PROJECT_DIR}/package.sh version))

Name:       %{name}
Version:    %{version}
Release:    %{release}
Summary:    A wrapper for cron
License:    GPL
URL:        https://gitlab.crowdnoetic.com/bash/%{name}
Group:      System
Packager:   Gayan Munasinghe
Requires:   s-nail
Requires:   esmtp-local-delivery

%description
A wrapper cron.

%prep

mkdir -p $RPM_BUILD_ROOT/opt/%{name}/
cp ${CI_PROJECT_DIR}/cronwrap $RPM_BUILD_ROOT/opt/%{name}/

%files
%attr(0555, root, root) /opt/%{name}/cronwrap

%pre

%clean
rm -rf $RPM_BUILD_ROOT/opt/%{name}

%changelog
* Sat Apr 15 2023 Gayan Munasinghe <gayan@outlook.com>
  - Update path
* Mon Mar 28 2023 Gayan Munasinghe <gayan@outlook.com>
  - Initial package release
